# GAEV - Gestion Automatique d'Environnement Virtuel

Le projet __GAEV__ (  **G**estion **A**utomatique d’**E**nvironnement **V**irtuel ) vise à établir une preuve de concept (POC) d’une infrastructure logicielle permettant de __créer__, __configurer__, __référencer__ et __stocker__ à la demande des environnements sous forme de __machines virtuelles__ lourdes déployables à multiple échelle.

La finalité d’un tel projet est de faciliter la __mise à disposition d’applications scientifiques__ pré-installées et configurées sur des machines virtuelles référencées. Cela s’inscrit dans une démarche qualité de science ouverte pour le __partage__ et la __reproductibilité__ tout en s’appuyant sur les principes __FAIR__.

C'est donc :

* Faciliter la mise à disposition d’applications scientifiques préinstallées et configurées sur des machines virtuelles référencées.  
* S’inscrire dans une démarche qualité de science ouverte pour le partage et la reproductibilité.  


## Structures

Le projet GAEV  a été financé par la [DipSO](https://ist.inrae.fr/list-a-inrae/dipso/) suite à l'appel au soutien de 2020 pour une durée d'un an (phase 1).  
Il est co-encadré par les CATIs [PROSODie](https://prosodie.cati.inrae.fr/) et [IMOTEP](https://imotep.inrae.fr/) et le DU de l'unité [BioSP](https://biosp.org).

## Participants

### Membres du projet

* [Loic Houde](https://loic.biosp.org)
* [Daniel Jacob]() (co-porteur phase 2)
* [Tovo Rabemanantsoa]()
* [Jean-Francois Rey](https://jeff.biosp.org) (porteur phase 1 et 2)

### Comment contribuer ?

Il est possible de contribuer de différentes manières :  

* En proposant un cas d’utilisation.
* En proposant des pistes ou des solutions pour le référencement des machines virtuelles.
* En contactant l’un des porteurs pour lui proposer sa participation active.


## Le Projet

Le rapport du projet est disponible ici et les différents POC et usecases sont disponibles dans le groupe [GAEV](https://forgemia.inra.fr/gaev).

![GAEV Phase 2](images/phase2_GAEV.png "GAEV phases")  
> Avancement du projet fin phase 1.

### Phase 1 (2020-2021)

La phase 1 du projet a consisté en :  

* Identification des différentes couches : i) Création, ii) Stockage et indexation, iii) Instanciation.  
* Veille technologique sur les outils open-sources couvrant l’ensemble des couches.  
* Choix des outils selon les critères suivants : libres, open-sources et possédant une forte communauté.  
* Choix des cas d’utilisation pour le POC représentatifs des applications susceptibles d’intéresser la communauté INRAE.  
* Développement des scripts de création des VM (de base et finales) basés sur nos choix d’outils (automatisation).  
* Choix pour le stockage des machines virtuelles (VM) selon leur type (VM dites de « base » ou VM dites « finales »).  
* Tests de déploiements (instanciation) sur un environnement cloud type Openstack.  
* Utiliser les VMs comme environnement de développement et tests pour les CI/CD GitLab.  

> [Rapport V1 2020 (phase 1)](documents/GAEV_rapport_2020.pdf) | [https://hal.inrae.fr/hal-03192628](https://hal.inrae.fr/hal-03192628)  

### Phase 2 (2021-...)

* Mener l’étude puis la réalisation de la partie référencement (métadonnées, indexation et référencement).  
* Création d'un site web pour le stockage avec référencement unique (Persistant ID) et l'indexation.  
* Faire évoluer le site web et les pipelines d'automatisations pour permettre la gestion automatique des environnements virtuels (phase 1) via cette interface (création, configuration, stockage, indexation et partage).  
* Elargir les collaborations : IFB (Biosphère/RAMbio), Inter-CATIs, Inter-instituts, ...


## Dépôts

### Documents / Délivrables

* [Rapport V1 2020 (phase 1)](documents/GAEV_rapport_2020.pdf) | [https://hal.inrae.fr/hal-03192628](https://hal.inrae.fr/hal-03192628)


### Création de VMs

* [Outils pour utiliser l'API Vagrant CLoud](https://forgemia.inra.fr/gaev/packer/vagrant-cloud_api)
* ["virtualbox" sour Windows](https://forgemia.inra.fr/gaev/packer/windows10)
* ["virtualbox" sous MacOs](https://forgemia.inra.fr/gaev/packer/macos)
* ["virtualbox" sous Centos](https://forgemia.inra.fr/gaev/packer/centos-dd8gb)
* ["virtualbox" sous Ubuntu](https://forgemia.inra.fr/gaev/packer/ubuntu1804-dd18gb)

Les boxes générées sont disponibles sur [https://app.vagrantup.com/GAEV/](https://app.vagrantup.com/GAEV/).  

### UseCases

* [VM sous MacOS + l'environnement R](https://forgemia.inra.fr/gaev/usecases/macosr)
* [VM sous Windows + l'environnement R](https://forgemia.inra.fr/gaev/usecases/windowsr)
* [ODAM](https://forgemia.inra.fr/gaev/usecases/odam-centos-virtualbox)
* [JupyterHub](https://forgemia.inra.fr/gaev/usecases/jupyterhub-ubuntu-vbox)

## LOGOS

<table>
<tr><td><img src="images/Republique_Francaise_RVB.jpg" align="left" alt="INRAE" width="120" /></td></tr>
<tr><td><img src="images/Logo-INRAE.jpg" align="left" alt="INRAE" width="120" /></td><td> <img src="images/DipSO_logo-gradient-rvb-scaled.jpg" align="right" alt="DipSO" width="120" /></td></tr>
<tr><td><img src="images/logo_imotep_officiel.png" align="left" alt="IMOTEP" width="120" /></td><td><img src="images/logo_PROSODIe.png" align="right" alt="PROSODIe" width="120" /></td></tr>
<tr><td><img src="images/biosp.png" align="left" alt="BioSP" width="120" /></td></tr> 
</table>




